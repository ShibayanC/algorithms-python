''' Implementation of min-Heap '''

class Node(object):
	def __init__(self, data):
		self.data = data
		self.left = None
		self.right = None

class HeapTree(object):
	def addNode(self, val):
		rt = Node(val)
		return rt

	def insertNode(self, root, val):
		if root is None:
			root = self.addNode(val)
		else:
			if val < 

	def printInOrder(self, root):
		if root is None:
			return None
		else:
			self.printInOrder(root.left)
			print root.data
			self.printInOrder(root.right)

	def printPreOrder(self, root):
		if root is None:
			return None
		else:
			print root.data
			self.printPreOrder(root.left)
			self.printPreOrder(root.right)

	def printPostOrder(self, root):
		if root is None:
			return None
		else:
			self.printPostOrder(root.right)
			self.printPostOrder(root.left)
			print root.data

def main():
	ht = HeapTree()
	root = ht.addNode(25)
	ht.insertNode(root, 12)
	ht.insertNode(root, 7)
	ht.insertNode(37)
	ht.insertNode(50)
	ht.insertNode(19)
	ht.insertNode(45)
	ht.insertNode(3)

	print "Printing Tree (InOrder): "
	
	
	return

if __name__ == "__main__":
	main()
