# To implement BFS, it uses queue

# Graph is in adjacent list representation
graph = {
        '1': ['2', '3', '4'],
        '2': ['5', '6'],
        '5': ['9', '10'],
        '4': ['7', '8'],
        '7': ['11', '12']
        }

def bfs(graph, start, end):
	queue = []
	queue.append(start)
	while queue:
		# print "Queue: ", queue
		path = queue.pop(0)
		# print "Path: ", path
		node = path[-1]
		# print "Node: ", node
		if node == end:
			return path
		for adjacent in graph.get(node, []):
			newPath = list(path)
			# print "NewPath-1: ", newPath
			newPath.append(adjacent)
			queue.append(newPath)
			# print "NewPath: ", newPath

def main():
	print bfs(graph, '1', '11')
	return

if __name__ == "__main__":
	main()
