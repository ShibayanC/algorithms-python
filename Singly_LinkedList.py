# Python code to implement singly linked list

class Node(object):
	def __init__(self, data, next):
		self.data = data
		self.next = next

class SingleLinkedList(object):
	head = None
	tail = None
	def show(self):
		print "Showing LL: "
		cur_node = self.head
		while cur_node is not None:
			print cur_node.data, "->",
			cur_node = cur_node.next
		print None

	def appendVal(self, data):
		node = Node(data, None)
		if self.head is None:
			self.head = self.tail = node
		else:
			self.tail.next = node
		self.tail = node

	def remove(self, node_val):
		cur_node = self.head
		prev_node = None
		while cur_node is not None:
			if cur_node.data == node_val:
				if prev_node is not None:
					prev_node.next = cur_node.next
				else:
					self.head = cur_node.next
			# Needed for the next iteration
			prev_node = cur_node
			cur_node = cur_node.next

def main():
	s = SingleLinkedList()
	s.appendVal(5)
	s.appendVal(12)
	s.appendVal(13)
	s.appendVal(25)
	s.appendVal(4)
	
	s.show()
	
	s.remove(31)
	s.remove(12)
	s.remove(2)

	s.show()
	return

if __name__ == "__main__":
	main()
