''' BST with LCA and Lookup options '''

import os
os.system('clear')

class Node(object):
	def __init__(self, data):
		self.data = data
		self.left = None
		self.right = None

class BinaryTree(object):
	def addNode(self, val):
		nd = Node(val)
		return nd

	def insertNode(self, root, val):
		if root is None:
			root = self.addNode(val)
		else:
			if val <= root.data:
				left = self.insertNode(root.left, val)
				root.left = left
			else:
				right = self.insertNode(root.right, val)
				root.right = right
		return root

	def printInOrder(self, root):
		if root is None:
			return None
		else:
			self.printInOrder(root.left)
			print root.data
			self.printInOrder(root.right)
		return

	def printPreOrder(self, root):
		if root is None:
			return None
		else:
			print root.data
			self.printPreOrder(root.left)
			self.printPostOrder(root.right)
		return

	def printPostOrder(self, root):
		if root is None:
			return None
		else:
			self.printPostOrder(root.right)
			self.printPostOrder(root.left)
			print root.data
		return

	def printTreeSize(self, root):
		if root is None:
			return 0
		else:
			return self.printTreeSize(root.left) + 1 + self.printTreeSize(root.right)

	def maxDepth(self, root):
		if (root is None) or (root.left is None) or (root.right is None):
			return 0
		else:
			leftDepth = self.maxDepth(root.left)
			rightDepth = self.maxDepth(root.right)
			if leftDepth > rightDepth:
				return leftDepth + 1
			else:
				return rightDepth + 1

	def minDepth(self, root):
		if (root is None) or (root.left is None) or (root.right is None):
			return 0
		else:
			leftDepth = self.minDepth(root.left)
			rightDepth = self.minDepth(root.right)
			if leftDepth < rightDepth:
				return leftDepth + 1
			else:
				return rightDepth + 1

	def lookUpVal(self, root, val):
		# print "Data: ", root.data
		if root is None:
			return None
		if root.data == val:
			return root
		if root.data > val:
			self.lookUpVal(root.left, val)
		if root.data < val:
			self.lookUpVal(root.right, val)
		return root

	def minTree(self, root):
		if root is None:
			return None
		while root.left is not None:
			root = root.left
		return root 

	def maxTree(self, root):
		if root is None:
			return None
		while root.right is not None:
			root = root.right
		return root

	def getLevel(self, root, initLevel, val):
		if root is None:
			return 0
		elif root.data == val:
			return initLevel
		elif val < root.data:
			return self.getLevel(root.left, initLevel + 1, val)
		else:
			return self.getLevel(root.right, initLevel + 1, val)

	def lca(self, root, n1, n2):
		if root is None:
			return None
		if root.data > n1 and root.data > n2:
			return self.lca(root.left, n1, n2)
		if root.data < n1 and root.data < n2:
			return self.lca(root.right, n1, n2)
		return root				

def main():
	bt = BinaryTree()
	root = bt.addNode(20)
	bt.insertNode(root, 8)
	bt.insertNode(root, 22)
	bt.insertNode(root, 4)
	bt.insertNode(root, 12)
	bt.insertNode(root, 10)
	bt.insertNode(root, 14)
	# bt.insertNode(root, 50)
	
	print "Tree In-Order: "
	bt.printInOrder(root)
	
	print "Tree Pre-Order: "
	bt.printPreOrder(root)
	
	print "Tree Post-Order: "
	bt.printPostOrder(root)
	
	s = bt.printTreeSize(root)
	print "Tree Size: ", s
	
	maxD = bt.maxDepth(root)
	print "Max Depth: ", maxD

	minD = bt.minDepth(root)
	print "Min Depth: ", minD
	
	lookUp = bt.lookUpVal(root, 4)
	print "Look Up result - 4: ", lookUp.data
	
	lookUp = bt.lookUpVal(root, 3)
	print "Look Up result - 3: ", lookUp.data

	lookUp = bt.lookUpVal(root, 22)
	print "Look Up result - 22: ", lookUp.data
	
	minTree = bt.minTree(root)
	print "Min of tree: ", (minTree, minTree.data)
	
	maxTree = bt.maxTree(root)
	print "Max of tree: ", (maxTree, maxTree.data)

	getlevel = bt.getLevel(root, 0, 22)
	print "Level in tree: ", getlevel

	lcaVal = bt.lca(root, 10, 22)
	print "LCA of BST: ", lcaVal.data
	return

if __name__ == "__main__":
	main()
