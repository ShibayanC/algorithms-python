'''
This code is for both the prob-4.1 and prob-4.3
'''

class Node(object):
	def __init__(self, val):
		self.data = val
		self.left = None
		self.right = None

class BinaryTree(object):
	def addNode(self, val):
		rt = Node(val)
		return rt

	def insertTreeNode(self, root, val):
		if root is None:
			rt = self.addNode(val)
			return rt
		else:
			if val <= root.data:
				lft = self.insertTreeNode(root.left, val)
				root.left = lft
			elif val > root.data:
				rt = self.insertTreeNode(root.right, val)
				root.right = rt
			return root

	def printInOrder(self, root):
		if root is None:
			return None
		else:
			self.printInOrder(root.left)
			print root.data
			self.printInOrder(root.right)

	def printPreOrder(self, root):
		if root is None:
			return None
		else:
			print root.data
			self.printPreOrder(root.left)
			self.printPreOrder(root.right)

	def printPostOrder(self, root):
		if root is None:
			return None
		else:
			self.printPostOrder(root.right)
			self.printPostOrder(root.left)
			print root.data

	def calLeftTreeHeight(self, root):
		if root is None:
			return 0
		else:
			return max(self.calLeftTreeHeight(root.left), self.calLeftTreeHeight(root.right)) + 1 

	def calRightTreeHeight(self, root):
		if root is None:
			return 0
		else:
			return max(self.calRightTreeHeight(root.left), self.calRightTreeHeight(root.right)) + 1
	
	def calTreeHt(self, root):
		if root is None:
			return 0
		else:
			return max(self.calTreeHt(root.left), self.calTreeHt(root.right)) + 1

	def isBalanced(self, root):
		if root is None:
			return None
		else:
			lTh = self.calLeftTreeHeight(root.left)
			rTh = self.calRightTreeHeight(root.right)
			if abs(lTh - rTh) <= 1:
				return True
			else:
				return False

	def treeHeight(self, root):
		if root is None:
			return None
		else:
			return max(self.calTreeHt(root.left), self.calTreeHt(root.right)) + 1

def main():
	bt = BinaryTree()
	
	'''								# Test cases for prob 4.1
	root = bt.addNode(50)
	bt.insertTreeNode(root, 12)
	bt.insertTreeNode(root, 62)
	bt.insertTreeNode(root, 73)
	bt.insertTreeNode(root, 6)
	bt.insertTreeNode(root, 17)
	bt.insertTreeNode(root, 23)
	bt.insertTreeNode(root, 87)
	bt.insertTreeNode(root, 3)
	bt.insertTreeNode(root, 95)
	'''
	
	arr = [5, 84, 30, 68, 21, 10, 92, 73, 11, 41, 36, 2]
	arr = sorted(arr)
	print "Array: ", arr
	mid = (len(arr) - 1)/2
	print "Array - mid: ", arr[mid]
	
	root = bt.addNode(50)
	
	k = 0
	for k in range(len(arr)):
		if k != mid:
			root.left = bt.insertTreeNode(root.left, arr[k])
			# bt.insertTreeNode(root, arr[k])  # Can be used without optimization
		if k > mid:
			root.right = bt.insertTreeNode(root.right, arr[k])
			# bt.insertTreeNode(root, arr[k])  # Can be used without optimization
	print "Tree In-Order: "
	bt.printInOrder(root)
	
	print "Tree Pre-Order: "
	bt.printPreOrder(root)
	
	print "Tree Post-Order: "
	bt.printPostOrder(root)
	
	print "Tree height: ", bt.treeHeight(root)        # Method call for solution of prob 4.3
	'''
	Algo for prob - 4.2 is insert the
	elements in the left of the arr in the left
	sub tree of the BST, mid as root and elem in
	the right of the arr in the right sub tree of
	the BST
	'''
	print "is Balanced ? ", bt.isBalanced(root)		# Method for the solution of prob 4.1
	return

if __name__ == "__main__":
	main()
