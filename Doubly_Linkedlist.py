# Implementation of doubly Linked List

class Node(object):
	def __init__(self, data, prev, next):
		self.data = data
		self.next = next
		self.prev = prev

class DoublyLinkedList(object):
	head = None
	tail = None
	def appendVal(self, data):
		new_node = Node(data, None, None)
		if self.head is None:
			self.head = self.tail = new_node
		else:
			new_node.prev = self.tail
			new_node.next = None
			self.tail.next = new_node
			self.tail = new_node
	def remove(self, data):
		cur_node = self.head
		while cur_node is not None:
			if cur_node.data == data:
				# if its not the first node
				if cur_node.prev is not None:
					cur_node.prev.next = cur_node.next
					cur_node.next.prev = cur_node.prev
				else:
					self.head = cur_node.next
					cur_node.next.prev = None
			cur_node = cur_node.next
	def show(self):
		print "Showing list data: "
		cur_node = self.head
		while cur_node is not None:
			print cur_node.prev.data if hasattr(cur_node.prev, "data") else None,
			print cur_node.data,
			print cur_node.next.data if hasattr(cur_node.next, "data") else None
			cur_node = cur_node.next	

def main():
	d = DoublyLinkedList()
	d.appendVal(5)
	d.appendVal(15)
	d.appendVal(25)
	d.appendVal(3)
	d.appendVal(17)
	
	d.show()
	
	d.remove(45)
	d.remove(25)
	d.remove(19)
	
	d.show()	
	return	
	
if __name__ == "__main__":
	main()
